#include <iostream>
using namespace std;

int main() {
	string name = "I love Parrots";
	int sizeOfName = name.size();
	
	cout << name << " the length of string is " << sizeOfName << "\n";
	cout << "First letter of string is - " << name[0] << ". Last letter of string is - " << name[name.size()-1] << "\n";

	return 0;

}